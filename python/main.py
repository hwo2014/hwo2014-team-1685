import collections
import json
import math
import random
import socket
import sys

import vector


class Track(dict):
    def __init__(self, *args, **keys):
        super(Track, self).__init__(*args, **keys)
        x = vector.Vector(self['startingPoint']['position']['x'], self['startingPoint']['position']['y'])
        d = vector.Vector(0, 1).rotate(-self['startingPoint']['angle'])
        start = [0.] * len(self['lanes'])
        for piece in self['pieces']:
            piece['x_cart'] = x
            piece['d_cart'] = d
            piece['start'] = start
            if 'length' in piece:
                x = x + piece['length'] * d
                piece['one_over_r'] = 0.
                start = [s + piece['length'] for s in start]
            else:
                o = piece['radius'] * d.rotate(math.copysign(90, piece['angle']))
                piece['c'] = x - o
                x = x - o + o.rotate(-piece['angle'])
                d = d.rotate(-piece['angle']).normalize()
                piece['length'] = piece['radius'] * math.pi * abs(piece['angle']) / 180.
                piece['one_over_r'] = 1. / piece['radius']
                start = [s + piece['length'] * (piece['radius'] + math.copysign(self['lanes'][l]['distanceFromCenter'], piece['angle'])) / piece['radius'] for l, s in enumerate(start)]

    def to_cart(self, car_piece_position):
        piece = self['pieces'][car_piece_position['pieceIndex']]
        distance_from_center = self['lanes'][car_piece_position['lane']['startLaneIndex']]['distanceFromCenter']
        if 'radius' in piece:
            lane_ratio = (piece['radius'] + distance_from_center * math.copysign(1., -piece['angle'])) / piece['radius']
            p = (car_piece_position['inPieceDistance'] / lane_ratio) / piece['length']
            x = piece['c'] + (piece['x_cart'] - piece['c']).rotate(- p * piece['angle']) * lane_ratio
            d = (piece['d_cart'].rotate(-p * piece['angle'])).normalize()
            r = piece['c'] - x
        else:
            x = piece['x_cart'] + car_piece_position['inPieceDistance'] * piece['d_cart']
            d = piece['d_cart']
            r = 1e10 * d.rotate(90)
            x += distance_from_center * d.rotate(-90)
        s = piece['start'][car_piece_position['lane']['startLaneIndex']] + car_piece_position['inPieceDistance']
        return x, d, r, s

    def one_over_r(self, piece_index, lane_index):
        piece = self['pieces'][piece_index]
        lane_ratio = 1.
        if 'radius' in piece:
            distance_from_center = self['lanes'][lane_index]['distanceFromCenter']
            lane_ratio = float(piece['radius'] + distance_from_center * math.copysign(1., -piece['angle'])) / piece['radius']
        return piece['one_over_r'] / lane_ratio

    def r(self, piece_index, lane_index):
        one_over_r = self.one_over_r(piece_index, lane_index)
        return 1. / one_over_r if one_over_r else float('inf')


class CarTelemetry(object):
    def __init__(self, car_id, track):
        self.car_id = car_id
        self.track = track

        # default values for differential camputatons
        self.angle = 0.
        self.omega = 0.
        self.x = vector.Vector(0., 0.)
        self.d = vector.Vector(1., 0.)
        self.v = vector.Vector(0., 0.)
        self.throttle = 1.
        self.turbo_available = False
        self.turbo = 1.
        self.turbo_factor = 1.
        self.slide_min_acceleration = 0.32
        self.future_v = []
        self.lap = 0
        self.lane = 0

    def update(self, gameTick, car, init=False):
        assert car['id']['color'] == self.car_id
        self.gameTick = gameTick
        self.piece_position = car['piecePosition']
        self.lap = car['piecePosition']['lap']
        self.lane = car['piecePosition']['lane']['startLaneIndex']
        angle = car['angle']
        omega = angle - self.angle
        self.omega_dot = omega_dot = omega - self.omega
        x, d, r, s = self.track.to_cart(self.piece_position)
        v = x - self.x
        self.a = a = v - self.v

        a_d = a.inner(self.d)
        a_r = a.inner(self.d.rotate(-90).normalize())
        if not init:
            print self.gameTick, car['piecePosition']['pieceIndex'], self.lane, self.lap, \
                '\t', self.throttle, self.turbo, self.turbo_available,\
                '\t', s, v.norm(), a_d, a_r, a.norm(), \
                '\t', angle, omega, omega_dot, \
                '\t', r.norm()
        self.x = x
        self.v = v
        self.a_r = a_r
        self.a_d = a_d
        self.angle = angle
        self.omega = omega
        self.d = d
        self.r = r
        self.s = s


class Ai(collections.Iterator):
    def __init__(self, car_id, track, cars_telemetry):
        self.car_id = car_id
        self.track = track
        self.cars_telemetry = cars_telemetry
        self.telemetry = self.cars_telemetry[self.car_id]

        self.min_throttle = 0.
        self.max_throttle = 1.
        self.strategy = 'qualifying'

        # estimated
        self.mass = 0.
        self.speed_friction = 0.
        self.slide_min_acceleration = 0.32
        self.slide_min_acceleration_stat = 0.
        self.slide_friction = 15.

    def update_telemetry(self, gameTick, data, init=False):
        for car in data:
            self.cars_telemetry[car['id']['color']].update(gameTick, car, init=init)

    def next_calibrating(self):
        # mass and speed_friction estimation
        if self.mass == 0.:
            if self.telemetry.v.norm() > 0.:
                self.mass = self.telemetry.a.inner(self.telemetry.d)
        elif self.speed_friction == 0.:
            self.speed_friction = (self.mass - self.telemetry.a.inner(self.telemetry.d)) / self.mass
            print '*** mass: %f, speed_friction: %f' % (self.mass, self.speed_friction)
        else:
            targets = self._compute_targets()
            self.telemetry.throttle = self._throttle_for_targets(targets)
            # if abs(self.telemetry.angle) < 0.1 and 0.95 * self.slide_min_acceleration < abs(self.telemetry.a_r) < 1.05 * self.slide_min_acceleration:
            #     self.slide_min_acceleration_stat = max(self.slide_min_acceleration_stat, abs(self.telemetry.a_r))
            #     self.slide_min_acceleration = self.slide_min_acceleration * 1.001
            # elif abs(self.telemetry.a_r) > self.slide_min_acceleration:
            #     self.slide_min_acceleration *= 0.999
            # self.telemetry.slide_min_acceleration = self.slide_min_acceleration

        return ('throttle', self.telemetry.throttle)

    def _compute_targets(self):
        current_piece = self.telemetry.piece_position['pieceIndex']
        current_lane = self.telemetry.piece_position['lane']['startLaneIndex']

        next_pieces = [current_piece + i for i in range(10) if current_piece + i < len(self.track['pieces'])]
        targets = [(self.track['pieces'][p]['start'][current_lane], (self.slide_min_acceleration * self.track.r(p, current_lane) ** 2 / (self.track.r(p, current_lane) - self.slide_friction)) ** 0.5) for p in next_pieces]
        return targets

    def _throttle_for_targets(self, targets):
        current_speed = self.mass * self.telemetry.throttle * self.telemetry.turbo + (1. - self.speed_friction) * self.telemetry.v.norm()
        if current_speed >= 1.01 * targets[0][1]:
            throttle = self.min_throttle
        elif current_speed >= 0.95 * targets[0][1]:
            throttle = targets[0][1] / self.mass / self.telemetry.turbo * self.speed_friction
            throttle *= (1. - max(0., (self.telemetry.angle - 40.) / 20.))
        else:
            throttle = self.max_throttle
        predicted_state = [(self.telemetry.s + current_speed, current_speed)]
        for i in range(40):
            speed_friction = (1. - self.speed_friction) * predicted_state[-1][1]
            predicted_state.append((predicted_state[-1][0] + speed_friction, speed_friction))
            p, s = predicted_state[-1]
            for pos, speed in targets[1:]:
                if p > pos and s > speed:
                    #print pos, speed, predicted_state
                    throttle = self.min_throttle
                    break
            if throttle == self.min_throttle:
                break

        return throttle

    def next_qualifying(self):
        #if self.telemetry.piece_position['pieceIndex'] > len(self.track['pieces']) - 4 and self.telemetry.turbo_available:
        #    self.telemetry.turbo_available = False
        #    return ('turbo', 'go')
        switch = random.randint(0, 300)
        if switch == 0:
            return ('switchLane', 'Right')
        elif switch == 1:
            return ('switchLane', 'Left')
        targets = self._compute_targets()
        self.telemetry.throttle = self._throttle_for_targets(targets)
        return ('throttle', self.telemetry.throttle)

    def next(self):
        if self.strategy == 'qualifying' and self.telemetry.lap < 1 and self.telemetry.piece_position['pieceIndex'] < 20:
            return self.next_calibrating()
        elif self.strategy == 'qualifying':
            return self.next_qualifying()
        elif self.strategy == 'competing':
            return self.next_competing()
        else:
            raise ValueError(self.strategy)

    def next_competing(self):
        switch = random.randint(0, 300)
        if switch == 0:
            return ('switchLane', 'Right')
        elif switch == 1:
            return ('switchLane', 'Left')
        targets = self._compute_targets()
        self.telemetry.throttle = self._throttle_for_targets(targets)
        return ('throttle', self.telemetry.throttle)

    def turbo_available(self, data):
        self.telemetry.turbo_available = True
        self.telemetry.turbo_factor = data['turboFactor']


class Bot(socket.socket):
    def __init__(self, host, port, name, key, track_name=None, car_count='1'):
        super(Bot, self).__init__()
        self.connect((host, int(port)))
        self.file = self.makefile()
        self._game_init(name, key, track_name, car_count)
        self._ai_setup()

    def get(self, msg_type, data=None):
        request_message = json.dumps({'msgType': msg_type, 'data': data}) + '\n'
        self.sendall(request_message)
        response_message = next(self.file)
        response = json.loads(response_message)
        # log anything that is not carPositions
        if 'msgType' not in response or response['msgType'] != 'carPositions':
            print '%r -> %r' % (request_message, response_message)
        return response

    def _game_init(self, name, key, track_name, car_count):
        # game init routine
        join = ('join', {'name': name, 'key': key})
        if track_name:
            join = ('joinRace', {'botId': {'name': name, 'key': key}, 'trackName': track_name, 'carCount': int(car_count)})
        self.get(*join)
        while True:
            response = self.get('throttle', 1.)
            if response['msgType'] == 'yourCar':
                self.my_car = response['data']
            elif response['msgType'] == 'gameInit':
                self.game_init = response['data']
                break
            else:
                print 'Got in game_init: %r' % response

    def _ai_setup(self):
        track = Track(self.game_init['race']['track'])
        cars_telemetry = {car['id']['color']: CarTelemetry(car['id']['color'], track) for car in self.game_init['race']['cars']}
        self.ai = Ai(self.my_car['color'], track, cars_telemetry)

    def _game_start(self):
        # game start routine
        while True:
            response = self.get('throttle', 1.)
            if response['msgType'] == 'carPositions':
                # update three times to start with no acceleration
                self.ai.update_telemetry(response.get('gameTick', -1), response['data'], init=True)
                self.ai.update_telemetry(response.get('gameTick', -1), response['data'], init=True)
                self.ai.update_telemetry(response.get('gameTick', -1), response['data'], init=True)
            elif response['msgType'] == 'gameStart':
                break
            else:
                print 'Got in game start: %r' % response

    def _game_race(self):
        for action, data in self.ai:
            response = self.get(action, data)
            if response['msgType'] == 'carPositions':
                self.ai.update_telemetry(response.get('gameTick', -1), response['data'])
            elif response['msgType'] == 'turboAvailable':
                self.ai.turbo_available(response['data'])
            elif response['msgType'] == 'turboStart':
                self.ai.telemetry.turbo = self.ai.telemetry.turbo_factor
            elif response['msgType'] == 'turboEnd':
                self.ai.telemetry.turbo = 1. 
            elif response['msgType'] == 'gameEnd':
                break

    def _game_end(self):
        response = self.get('throttle', 1.)
        assert response['msgType'] == 'tournamentEnd'
        try:
            response = self.get('throttle', 1.)
        except StopIteration:
            pass
        else:
            raise ValueError(response)

    def run_until_complete(self):
        if self.game_init['race']['raceSession'].get('quickRace', False):
            strategies = ('qualifying',)
        else:
            strategies = ('qualifying', 'competing')

        for strategy in strategies:
            print '**** strategy:', strategy
            self.ai.strategy = strategy
            self._game_start()
            self._game_race()

        self._game_end()

if __name__ == '__main__':
    bot = Bot(*sys.argv[1:])
    bot.run_until_complete()
